package com.wix.task;

import com.wix.task.input.CategoryConsoleReader;
import com.wix.task.model.Category;
import com.wix.task.printer.IterativeCategoryPrinter;
import com.wix.task.printer.RecursiveCategoryPrinter;
import com.wix.task.storage.CategoryTreeStorage;
import com.wix.task.storage.InMemoryCategoryTreeStorage;

public class Application {

	public static void main(String[] args) {
		runWithTestData();
	}
	
	private static void runWithTestData() {
		CategoryTreeStorage storage = createTestStorage();
		
		RecursiveCategoryPrinter recursivePrinter = new RecursiveCategoryPrinter(storage);
		IterativeCategoryPrinter iterativePrinter = new IterativeCategoryPrinter(storage);
		
		print(recursivePrinter, iterativePrinter);
		
		CategoryConsoleReader categoryReader = new CategoryConsoleReader(storage);
		
		while(true) {
			categoryReader.read();
			print(recursivePrinter, iterativePrinter);
		}
	}

	private static void print(RecursiveCategoryPrinter recursivePrinter, IterativeCategoryPrinter iterativePrinter) {
		System.out.println(" **** Recursive: ");
		recursivePrinter.print();
		
		System.out.println(" **** Iterative: ");
		iterativePrinter.print();
		
		System.out.println("-------------------");
	}

	private static CategoryTreeStorage createTestStorage() {
		CategoryTreeStorage storage = new InMemoryCategoryTreeStorage();
		
		Category root0 = new Category("Cat1", null);
		Category root2 = new Category("Cat2", null);
		Category root1 = new Category("Cat3", null);
		storage.store(root0)
				.store(root1)
				.store(root2);
		
		Category subcat2 = new Category("SubCat2", root0.id);
		Category subcat4 = new Category("SubCat4", root2.id);
		
		storage.store(new Category("SubCat1", root0.id))
				.store(subcat2)
				.store(new Category("SubCat3", root1.id))
				.store(subcat4)
				.store(new Category("SubCat5", root2.id))
				.store(new Category("SubSubCat", subcat2.id))
				.store(new Category("SubSubCat", subcat2.id))
				.store(new Category("SubSubCat", subcat4.id))
				.store(new Category("SubSubCat", subcat4.id))
				;
		
		return storage;
	}

}
