package com.wix.task.input;

import java.util.Scanner;

import com.wix.task.model.Category;
import com.wix.task.storage.CategoryTreeStorage;

public class CategoryConsoleReader {

	private CategoryTreeStorage categoryTreeStorage;
	private Scanner scanner = new Scanner(System.in);
	
	public CategoryConsoleReader(CategoryTreeStorage categoryTreeStorage) {
		this.categoryTreeStorage = categoryTreeStorage;
	}
	
	public void read() {
		System.out.println("Enter New Category name: ");
		String categoryName = scanner.next();
		
		System.out.println("Enter " + categoryName + " parent ID: ");
		
		Integer parentID = null;
		
		while (parentID == null) {
			try {
				String parentIDString = scanner.next();
				parentID = Integer.decode(parentIDString);
			} catch (Exception e) {
				System.out.println("Input error: should be integer");
			}
		}	
		
		Category category = new Category(categoryName, parentID);
		categoryTreeStorage.store(category);
		System.out.println("Added category: " + category.toString());
	}
}
