package com.wix.task.printer;

import java.util.List;

import com.wix.task.model.Category;
import com.wix.task.storage.CategoryTreeStorage;

public class RecursiveCategoryPrinter extends AbstractCategoryPrinter {
	
	public RecursiveCategoryPrinter(CategoryTreeStorage categoryTreeStorage) {
		super(categoryTreeStorage);
	}

	public void print() {
		List<Category> orphans = categoryTreeStorage.getOrphans();
		printTreeRecursively(orphans, 0);
	}

	private void printTreeRecursively(List<Category> nodes, int depth) {
		for (Category node : nodes) {
			printNode(node.toString(), depth);
			
			printTreeRecursively(categoryTreeStorage.getChildren(node), depth + 1);
		}
	}
	
}
