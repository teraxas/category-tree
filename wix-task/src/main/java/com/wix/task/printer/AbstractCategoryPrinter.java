package com.wix.task.printer;

import com.wix.task.storage.CategoryTreeStorage;

public abstract class AbstractCategoryPrinter {

	private static final String NODE_SEPPARATOR = "- - ";
	private static final String DEPTH_SEPPARATOR = "  ";

	protected CategoryTreeStorage categoryTreeStorage;

	public AbstractCategoryPrinter(CategoryTreeStorage categoryTreeStorage) {
		this.categoryTreeStorage = categoryTreeStorage;
	}
	
	public abstract void print();

	protected void printNode(String nodeName, int depth) {
		StringBuilder line = new StringBuilder();
		for (int i = 0; i< depth; i++) {
			line.append(DEPTH_SEPPARATOR);
		}
		line.append(NODE_SEPPARATOR);
		line.append(nodeName);
		
		System.out.println(line.toString());
	}
	
}
