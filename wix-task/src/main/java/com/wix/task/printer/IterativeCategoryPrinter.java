package com.wix.task.printer;

import java.util.List;
import java.util.Optional;

import com.google.common.collect.Lists;
import com.wix.task.model.Category;
import com.wix.task.storage.CategoryTreeStorage;

public class IterativeCategoryPrinter extends AbstractCategoryPrinter {
	
	public IterativeCategoryPrinter(CategoryTreeStorage categoryTreeStorage) {
		super(categoryTreeStorage);
	}
	
	@Override
	public void print() {
		List<Integer> visited = Lists.newArrayList();
		
		for (Category rootNode : categoryTreeStorage.getOrphans()) {
			int depth = 0;
			boolean backToRoot = false;
			
			printNode(rootNode.toString(), depth);
			visited.add(rootNode.id);
			
			Category currentNode = rootNode;
			
			while (!backToRoot) {
				Optional<Category> anyChild = categoryTreeStorage.getChildren(currentNode)
						.stream()
						.filter(n -> !visited.contains(n.id))
						.findFirst();
				
				if (anyChild.isPresent()) { // Go deeper
					depth++;
					currentNode = anyChild.get();
					printNode(currentNode.toString(), depth);
					visited.add(currentNode.id);
				} else if (currentNode.parentID != null) { // Go back
					depth--;
					currentNode = categoryTreeStorage.get(currentNode.parentID);
				} else {
					backToRoot = false;
				}
				
			}
		}
	}
	
}
