package com.wix.task.model;

public class Category {

	public Integer id;
	public Integer parentID;
	public String name;
	
	public Category(String name, Integer parentID) {
		this.name = name;
		this.parentID = parentID;
	}

	@Override
	public String toString() {
		return name + " (ID: " + id + ", parentID: " + parentID + ")";
	}
	
}
