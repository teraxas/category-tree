package com.wix.task.storage;

import java.util.List;

import com.wix.task.model.Category;

public interface CategoryTreeStorage {
	
	CategoryTreeStorage store(Category category);
	List<Category> getChildren(Category category);
	List<Category> getOrphans();
	Category get(Integer id);
	
}
