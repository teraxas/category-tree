package com.wix.task.storage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.wix.task.model.Category;

public class InMemoryCategoryTreeStorage implements CategoryTreeStorage {

	private Multimap<Integer, Category> categoriesByParent = Multimaps.newMultimap(new HashMap<>(), ArrayList::new);
	private Map<Integer, Category> categoriesById = Maps.newHashMap();
	
	private Integer idCounter = 0;
	
	@Override
	public CategoryTreeStorage store(Category category) {
		category.id = idCounter;
		idCounter++;
		categoriesByParent.put(category.parentID, category);
		categoriesById.put(category.id, category);
		
		return this;
	}
	
	@Override
	public Category get(Integer id) {
		return categoriesById.get(id);
	}

	public List<Category> getChildren(Category category) {
		return (List<Category>) categoriesByParent.get(category.id);
	}

	@Override
	public List<Category> getOrphans() {
		return (List<Category>) categoriesByParent.get(null);
	}

	public void setCategoriesByParent(Multimap<Integer, Category> categoriesByParent) {
		this.categoriesByParent = categoriesByParent;
	}
	
	 
}
